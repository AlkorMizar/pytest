# -*- coding: utf8 -*-
# !/usr/bin/env python
import json as js
import shutil
import time
import os
from datetime import datetime
from json.decoder import JSONDecodeError
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, Callable, Optional

import uvicorn

# import guvicorn

from fastapi import (
    BackgroundTasks,
    Body,
    Depends,
    FastAPI,
    File,
    Form,
    Header,
    Request,
    Response,
    UploadFile,
)
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from loguru import logger

security = HTTPBasic()
app = FastAPI(title="docs api")



@app.get("/ping")
async def ping(
    # body: Any = Body(...),
    # file: UploadFile = File(...)
    callback: Optional[str] = Header(None),
):
    logger.info("Get simple get request for Ping - Ppong")
    return "pong"


if __name__ == "__main__":
    uvicorn.run("main:app", port=5000, log_level="info")
